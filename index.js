process.env['ELECTRON_DISABLE_SECURITY_WARNINGS'] = 'true';

const
    { app, BrowserWindow } = require('electron'),
    Store = require('electron-store'),
    path = require('path'),
    Parcel = app.isPackaged ? undefined : require('parcel-bundler'),
    {
        port,
        icon,
        enableTitleBar,
        enableMenuBar
    } = require('./config.json');

app.once('ready', async () => {
    Store.initRenderer();
    const mainWindow = new BrowserWindow({
        ...enableTitleBar === false ? {
            frame: false
        } : {},
        icon,
        webPreferences: {
            preload: path.join(__dirname, './preload.js')
        },
        show: false
    });
    if(enableMenuBar === false) mainWindow.setMenuBarVisibility(false);
    if(app.isPackaged) await mainWindow.loadFile('./dist/index.html');
    else {
        mainWindow.openDevTools();
        await new Parcel(path.join(__dirname, './index.html')).serve(port);
        await mainWindow.loadURL(`http://localhost:${port}/`);
    }
    mainWindow.show();
});