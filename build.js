const
    fs = require('fs'),
    Parcel = require('parcel-bundler'),
    electronBuilder = require('electron-builder'),
    {
        name
    } = require('./package.json'),
    {
        icon, // TODO
        build: {
            ignoredDirectories
        }
    } = require('./config.json');

(async () => {
    fs.rmdirSync('./dist', { recursive: true });
    await new Parcel('index.html', { publicURL: '.' }).bundle();
    await electronBuilder.build({
        targets: {
            'linux': electronBuilder.Platform.LINUX.createTarget(),
            'win32': electronBuilder.Platform.WINDOWS.createTarget(),
            'darwin': electronBuilder.Platform.MAC.createTarget()
        }[process.platform],
        config: {
            files: fs.readdirSync('.', { withFileTypes: true })
                .filter(item => ![
                    '.git',
                    '.gitignore',
                    'build.js',
                    'README.md',
                    'yarn.lock',
                    ...fs.readFileSync('./.gitignore', 'utf8')
                        .split('\n')
                        .filter(path => !['dist', 'node_modules'].includes(path))
                ].includes(item.name))
                .map(item => `${item.name}${item.isDirectory() ? '/**' : ''}`),
            directories: {
                output: 'build'
            },
            win: {
                target: [{
                    target: 'nsis',
                    arch: 'x64'
                }],
            },
            linux: {
                target: [{
                    target: 'AppImage',
                    arch: 'x64'
                }],
            },
            mac: {
                target: [{
                    target: 'dmg'
                }]
            }
        }
    });
    process.exit();
})();